import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.apache.spark.SparkContext
import org.apache.spark.ml.feature.{HashingTF, IDF, RegexTokenizer}
import org.apache.spark.ml.linalg.SparseVector
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.{asc, col, desc, size, udf}
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

import java.io._

object RecommenderSystem {

  def main(args: Array[String]) {

    val ids = Array(3875, 23, 774, 819, 1451, 2019)

    val spark: SparkSession = SparkSession
      .builder
      .master("local")
      .getOrCreate
    val sc: SparkContext = spark.sparkContext

    sc.setLogLevel("ERROR")

    val schema = ScalaReflection
      .schemaFor[Course]
      .dataType
      .asInstanceOf[StructType]

    val df = spark
      .read
      .schema(schema)
      .json("DO_record_per_line.json")

    val tokenizer = new RegexTokenizer()
      .setInputCol("desc")
      .setOutputCol("words")
      .setPattern("[^A-Za-zА-ЯЁа-яё]")

    val wordsData = tokenizer
      .transform(df)
      .filter(size(col("words")) > 0)

    val hashingTF = new HashingTF()
      .setInputCol("words")
      .setOutputCol("rawFeatures")
      .setNumFeatures(10000)

    val featuredData = hashingTF.transform(wordsData)

    val idf = new IDF()
      .setInputCol("rawFeatures")
      .setOutputCol("features")

    val idfModel = idf.fit(featuredData)

    val rescaledData = idfModel.transform(featuredData)

    val recommendedCourses = ids.map(rescaledData.findSuitableCourses).toMap

    writeJsonFile("recommendedCourses.json", recommendedCourses)
  }

  def writeJsonFile(filename: String, obj: Any): Unit =
    new ObjectMapper()
      .registerModule(DefaultScalaModule)
      .writerWithDefaultPrettyPrinter
      .writeValue(
        new BufferedWriter(
          new FileWriter(
            new File(filename))),
        obj)

  implicit class ExtendedDF(df: DataFrame) {
    def findSuitableCourses(id: Int): (Int, Array[Int]) = {
      val baseCourse: Row = df
        .select("id", "name", "lang", "features")
        .filter(col("id") === id)
        .first()

      val baseCourseLang = baseCourse(2)

      val baseCourseVector: SparseVector = baseCourse.getAs[SparseVector](3)

      val cosineSimilarityWithBaseCourseVector: SparseVector => Double = (vector: SparseVector) => cosineSimilarity(baseCourseVector, vector)
      val func: UserDefinedFunction = udf(cosineSimilarityWithBaseCourseVector)

      val recommendedCourses = df
        .select("id", "name", "lang", "features")
        .filter(col("lang") === baseCourseLang && col("id") =!= id)
        .withColumn("cosineSimilarity", func(col("features")))
        .orderBy(desc("cosineSimilarity"), asc("name"), asc("id"))
        .limit(10)
        .collect
        .map(_.getInt(0))

      id -> recommendedCourses
    }
  }

  def cosineSimilarity(vector1: SparseVector, vector2: SparseVector): Double =
    dotProduct(vector1, vector2) / vectorLengthsProduct(vector1, vector2)

  def dotProduct(vector1: SparseVector, vector2: SparseVector): Double = (vector1.indices ++ vector2.indices)
    .to[Set]
    .map(i => vector1(i) * vector2(i))
    .sum

  def moduleVector(vector: SparseVector): Double = vector
    .values
    .map(x => x * x)
    .sum

  def vectorLengthsProduct(vector1: SparseVector, vector2: SparseVector): Double =
    math.sqrt(moduleVector(vector1) * moduleVector(vector2))

}
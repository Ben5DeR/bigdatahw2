case class Course(lang: String,
                  name: String,
                  cat: String,
                  provider: String,
                  id: Int,
                  desc: String,
                 ) {
}